package com.example.accessingdatajpa.repositories;

import com.example.accessingdatajpa.datamodel.assemblers.PersonDomainDataAssembler;
import com.example.accessingdatajpa.repositories.jpa.AddressJpaRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class AddressRepository {

	@Autowired
	AddressJpaRepository addressJpaRepository;
	@Autowired
	PersonDomainDataAssembler addressAssembler;
/*
	public Address save( Address address ) {
		AddressJpa addressJpa = addressAssembler.toData(address);
		
		addressJpa = addressJpaRepository.save( addressJpa );
		
		return addressAssembler.toDomain(addressJpa);
	}

	public Optional<Address> findByCode(String code) {
		Optional<AddressJpa> opAddress = addressJpaRepository.findById(code);

		if ( opAddress.isPresent() ) {
			Address address = addressAssembler.toDomain(opAddress.get());
			return Optional.of( address );
		}
		else
			return Optional.empty();
	}
	*/
}