package com.example.accessingdatajpa.repositories.jpa;

import java.util.Optional;
import java.util.List;

import com.example.accessingdatajpa.datamodel.PersonJpa;
import com.example.accessingdatajpa.domain.valueobjects.PersonId;

import org.springframework.data.repository.CrudRepository;

public interface PersonJpaRepository extends CrudRepository<PersonJpa, PersonId> {

	List<PersonJpa> findAll();

	List<PersonJpa> findByLastName(String lastName);

	Optional<PersonJpa> findById(PersonId id);
}