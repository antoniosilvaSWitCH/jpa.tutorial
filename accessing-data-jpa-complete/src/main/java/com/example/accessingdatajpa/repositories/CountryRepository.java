package com.example.accessingdatajpa.repositories;

import java.util.Optional;

import com.example.accessingdatajpa.datamodel.CountryJpa;
import com.example.accessingdatajpa.datamodel.assemblers.CountryDomainDataAssembler;
import com.example.accessingdatajpa.domain.entities.Country;
import com.example.accessingdatajpa.repositories.jpa.CountryJpaRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CountryRepository {

	@Autowired
	CountryJpaRepository countryJpaRepository;
	@Autowired
	CountryDomainDataAssembler countryAssembler;

	public Country save( Country country ) {
		CountryJpa countryJpa = countryAssembler.toData(country);

		CountryJpa savedCountryJpa = countryJpaRepository.save( countryJpa );

		return countryAssembler.toDomain(savedCountryJpa);
	}

	public Optional<Country> findByCode(String code) {
		Optional<CountryJpa> opCountry = countryJpaRepository.findByCode(code);

		if ( opCountry.isPresent() ) {
			Country country = countryAssembler.toDomain(opCountry.get());
			return Optional.of( country );
		}
		else
			return Optional.empty();
	}
}