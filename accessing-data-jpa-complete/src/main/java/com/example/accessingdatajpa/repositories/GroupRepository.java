package com.example.accessingdatajpa.repositories;

import java.util.ArrayList;
import java.util.Optional;
import java.util.List;

import com.example.accessingdatajpa.datamodel.GroupJpa;
import com.example.accessingdatajpa.datamodel.AdminJpa;
import com.example.accessingdatajpa.datamodel.assemblers.GroupDomainDataAssembler;
import com.example.accessingdatajpa.domain.entities.Group;
import com.example.accessingdatajpa.domain.valueobjects.GroupId;
import com.example.accessingdatajpa.domain.valueobjects.PersonId;
import com.example.accessingdatajpa.repositories.jpa.AdminJpaRepository;
import com.example.accessingdatajpa.repositories.jpa.GroupJpaRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class GroupRepository {

	@Autowired
	GroupJpaRepository groupJpaRepository;
	@Autowired
	AdminJpaRepository adminJpaRepository;

	@Autowired
	GroupDomainDataAssembler groupAssembler;

	public Group save( Group group ) {
		GroupJpa groupJpa = groupAssembler.toData(group);

		groupJpa = groupJpaRepository.save( groupJpa );

		PersonId personId = group.getAdministrators().get(0);
		addAndSaveAdmin( group, personId);
		
		return groupAssembler.toDomain(groupJpa);
	}

	public Optional<Group> findById(GroupId id) {
		Optional<GroupJpa> opGroupJpa = groupJpaRepository.findById(id);

		if(opGroupJpa.isPresent()) {
			GroupJpa groupJpa = opGroupJpa.get();
			
			Group group = groupAssembler.toDomain(groupJpa);
			return Optional.of(group);
		}
		else
			return Optional.empty();
	}

	public List<Group> findByName(String name) {
		List<GroupJpa> setGroupJpa = groupJpaRepository.findByName(name);

		List<Group> setGroup = new ArrayList<Group>();
		for( GroupJpa groupJpa : setGroupJpa ) {
			Group group = groupAssembler.toDomain(groupJpa);
			setGroup.add(group);
		}

		return setGroup;
	}

	public List<Group> findAll() {
		List<GroupJpa> setGroupJpa = groupJpaRepository.findAll();

		List<Group> setGroup = new ArrayList<Group>();
		for( GroupJpa groupJpa : setGroupJpa ) {
			Group group = groupAssembler.toDomain(groupJpa);
			setGroup.add(group);
		}

		return setGroup;
	}

	public boolean addAndSaveAdmin(Group group, PersonId adminId) {
		GroupJpa groupJpa = groupAssembler.toData(group);

		AdminJpa adminJpa = new AdminJpa(groupJpa, adminId );

		adminJpaRepository.save( adminJpa );

		return true;
	}

	public List<PersonId> findAdminsById( GroupId id ) {
		return groupJpaRepository.findAllById(id);
	}
}