package com.example.accessingdatajpa.repositories.jpa;

import java.util.Optional;
import java.util.List;

import com.example.accessingdatajpa.datamodel.GroupJpa;
import com.example.accessingdatajpa.domain.valueobjects.GroupId;
import com.example.accessingdatajpa.domain.valueobjects.PersonId;

import org.springframework.data.repository.CrudRepository;

public interface GroupJpaRepository extends CrudRepository<GroupJpa, GroupId> {

	List<GroupJpa> findAll();

	List<GroupJpa> findByName(String lastName);

	Optional<GroupJpa> findById(GroupId id);

	List<PersonId> findAllById( GroupId id);
}