package com.example.accessingdatajpa.datamodel.assemblers;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import com.example.accessingdatajpa.DTO.AddressDTO;
import com.example.accessingdatajpa.datamodel.AddressJpa;
import com.example.accessingdatajpa.datamodel.PersonJpa;
import com.example.accessingdatajpa.domain.entities.Person;

@Service
public class PersonDomainDataAssembler {

	public PersonJpa toData( Person person ) {
		PersonJpa personJpa = new PersonJpa( person.getId(), person.getFirstName(), person.getLastName() );

		List<AddressDTO> addressesDTO = person.getAddresses();
		ArrayList<AddressJpa> addressesJpa = new ArrayList<AddressJpa>();

		for( AddressDTO address : addressesDTO ) {
			AddressJpa addressJpa = new AddressJpa(address.getStreet(), address.getCity(), address.getPostalCode(), address.getCountryCode(), personJpa);
			addressesJpa.add(addressJpa);
		}
		personJpa.setAddresses(addressesJpa);

		return personJpa;
	}

	public Person toDomain( PersonJpa personJpa ) {
		return new Person( personJpa.getId(), personJpa.getFirstName(), personJpa.getLastName() );
	}
}