package com.example.accessingdatajpa.datamodel;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.example.accessingdatajpa.domain.valueobjects.GroupId;

import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Entity
@Table(name="groups")
public class GroupJpa {
	@Getter
	@EmbeddedId
	private GroupId id;

	@Getter
	private String name;

	@Getter
	@OneToMany(mappedBy = "id.groupJpa", cascade = CascadeType.ALL)
	private List<AdminJpa> administrators;

	public GroupJpa(long id, String name ) {
		this.id = new GroupId(id);
		this.name = name;
		this.administrators = new ArrayList<AdminJpa>();
	}

	public GroupJpa(GroupId id, String name ) {
		this.id = id;
		this.name = name;
		this.administrators = new ArrayList<AdminJpa>();
	}

	public String toString() {
        return "Group {" +
                "id='" + id.toString() + '\'' +
                '}';
	}
}
