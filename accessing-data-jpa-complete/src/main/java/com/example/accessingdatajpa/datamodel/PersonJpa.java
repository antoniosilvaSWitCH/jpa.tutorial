package com.example.accessingdatajpa.datamodel;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.example.accessingdatajpa.domain.valueobjects.PersonId;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor

@Entity
@Table(name="persons")
public class PersonJpa {
	@Id
	@Embedded
	private PersonId id;
	private String firstName;
	private String lastName;

	// if only one address is required, two alternatives are possible:
	//@Convert(converter = AddressConverter.class)
	//or
	//@OneToOne(cascade = CascadeType.ALL)
	//@JoinColumn(name = "address_id", referencedColumnName = "id")
	//private AddressJpa address;

	@OneToMany(mappedBy = "person", cascade = CascadeType.ALL)
	private List<AddressJpa> addresses;

	public PersonJpa(long id, String firstName, String lastName) {
		this.id = new PersonId(id);

		this.firstName = firstName;
		this.lastName = lastName;

		addresses = new ArrayList<AddressJpa>();
	}

	public PersonJpa(PersonId id, String firstName, String lastName) {
		this.id = id;

		this.firstName = firstName;
		this.lastName = lastName;

		addresses = new ArrayList<AddressJpa>();
	}

	public String toString() {
        return "Person {" +
                "id='" + id.toString() + '\'' +
                '}';
	}
}
