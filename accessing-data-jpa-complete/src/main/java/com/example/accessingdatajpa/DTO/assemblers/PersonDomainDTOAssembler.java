package com.example.accessingdatajpa.DTO.assemblers;

import com.example.accessingdatajpa.DTO.PersonDTO;
import com.example.accessingdatajpa.domain.valueobjects.PersonId;

import org.springframework.stereotype.Service;

@Service
public class PersonDomainDTOAssembler {

    private PersonDomainDTOAssembler() {
    }

    public PersonDTO toDTO(PersonId id, String firstName, String lastName ) {
        return new PersonDTO(id, firstName, lastName);
    }
}