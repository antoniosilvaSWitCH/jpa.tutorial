package com.example.accessingdatajpa.DTO;

import com.example.accessingdatajpa.domain.valueobjects.PersonId;

import org.springframework.hateoas.RepresentationModel;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ToString
public class PersonDTO extends RepresentationModel<PersonDTO> {
    @Getter
    PersonId id;
    @Getter
    String firstName;
    @Getter
    String lastName;
}
