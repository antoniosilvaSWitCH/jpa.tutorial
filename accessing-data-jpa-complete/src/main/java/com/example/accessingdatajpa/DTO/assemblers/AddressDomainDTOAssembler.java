package com.example.accessingdatajpa.DTO.assemblers;

import com.example.accessingdatajpa.DTO.AddressDTO;

public class AddressDomainDTOAssembler {

    private AddressDomainDTOAssembler() {
    }

    public static AddressDTO toDTO(String street, String city, String postalCode, String countryCode ) {
        return new AddressDTO(street, city, postalCode, countryCode);
    }
}