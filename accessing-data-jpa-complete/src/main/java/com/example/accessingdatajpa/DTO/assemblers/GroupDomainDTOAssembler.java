package com.example.accessingdatajpa.DTO.assemblers;

import com.example.accessingdatajpa.DTO.GroupDTO;
import com.example.accessingdatajpa.domain.valueobjects.GroupId;

import org.springframework.stereotype.Service;

@Service
public class GroupDomainDTOAssembler {

    private GroupDomainDTOAssembler() { 
    }

    public GroupDTO toDTO(GroupId id, String name ) {
        return new GroupDTO(id, name);
    }
}