package com.example.accessingdatajpa;

import java.util.Optional;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.example.accessingdatajpa.domain.entities.Country;
import com.example.accessingdatajpa.DTO.AddressDTO;
import com.example.accessingdatajpa.DTO.PersonDTO;
import com.example.accessingdatajpa.domain.valueobjects.GroupId;
import com.example.accessingdatajpa.domain.valueobjects.PersonId;
import com.example.accessingdatajpa.services.PersonService;
import com.example.accessingdatajpa.services.CountryService;
import com.example.accessingdatajpa.services.GroupService;

@SpringBootApplication
public class AccessingDataJpaApplication {

	private static final Logger log = LoggerFactory.getLogger(AccessingDataJpaApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(AccessingDataJpaApplication.class);
	}

	@Bean
	public CommandLineRunner demo(CountryService countryService, PersonService personService, GroupService groupService ) {
		return (args) -> {
			// create and save a few countries
			Country countryPT = countryService.createAndSaveCountry("PT", "Portugal");
			Country countryUSA = countryService.createAndSaveCountry("USA", "United States of America");
			//Country countryUK = countryService.createAndSaveCountry("UK", "United Kingdom");

			// create and save a few persons
			personService.createAndSavePerson(1L,"Jack", "Bauer");
			personService.createAndSavePerson(2L,"Chloe", "O'Brian");
			personService.createAndSavePerson(3L,"Kim", "Bauer");
			personService.createAndSavePerson(4L,"David", "Palmer");
			personService.createAndSavePerson(5L,"Michelle", "Dessler");

			// fetch all persons
			log.info("Persons found with findAll():");
			log.info("-------------------------------");
			for (PersonDTO person : personService.findAll()) {
				log.info(person.toString());
			}
			log.info("");

			// fetch an individual person by ID
			PersonId personId = new PersonId(1L);
			Optional<PersonDTO> opPerson = personService.findById(personId);
			if( opPerson.isPresent() ) {
				PersonDTO person = opPerson.get();
				log.info("Person found with findById(1L):");
				log.info("--------------------------------");
				log.info(person.toString());
				log.info("");
			}

			// fetch persons by last name
			log.info("Persons found with findByLastName('Bauer'):");
			log.info("--------------------------------------------");
			List<PersonDTO> personsDTO = personService.findByLastName("Bauer");
			for(PersonDTO personDTO : personsDTO ) {
				log.info(personDTO.toString());
			};

			personService.addAddressToPerson( personId, "rua de lá", "Porto", "4200-072", countryPT.getCode());
			personService.addAddressToPerson( personId, "rua de ló", "Porto", "4200-072", countryPT.getCode());
			personService.addAddressToPerson( new PersonId(3L),"Broadway", "NYC", "4300-567", countryUSA.getCode());
			personService.addAddressToPerson( new PersonId(4L),"Rua 16", "Espinho", "4500-240", countryPT.getCode());
			personService.addAddressToPerson( new PersonId(4L),"Rua 16", "Espinho", "4500-240", countryPT.getCode());

			// fetch addresses from person with id = personId = 1L
			log.info("Addresses of Person found with findById( personId ):");
			log.info("--------------------------------------------");
			List<AddressDTO> addressesDTO = personService.getAddresses(personId);
			for( AddressDTO addressDTO : addressesDTO ) {
				log.info( addressDTO.toString() );
			}

			// create and save a few groups
			groupService.createAndSaveGroup(1L,"SWitCH", new PersonId(1L));
			groupService.createAndSaveGroup(2L,"MEI", new PersonId(1L));
			groupService.createAndSaveGroup(3L,"LEI", new PersonId(4L));

			groupService.addAdminToGroup(new GroupId(1L), new PersonId(2L));
			groupService.addAdminToGroup(new GroupId(2L), new PersonId(3L));
			groupService.addAdminToGroup(new GroupId(2L), new PersonId(4L));
		};
	}

}
