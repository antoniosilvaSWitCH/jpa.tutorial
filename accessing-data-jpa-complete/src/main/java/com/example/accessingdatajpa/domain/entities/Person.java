package com.example.accessingdatajpa.domain.entities;

import java.util.ArrayList;
import java.util.List;

import com.example.accessingdatajpa.DTO.AddressDTO;
import com.example.accessingdatajpa.domain.valueobjects.PersonId;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString

public class Person {
	@Getter
	private final PersonId id;
	@Getter
	private String firstName;
	@Getter
	private String lastName;
	@Setter
	private List<Address> addresses = new ArrayList<Address>();

	public Person(long id, String firstName, String lastName) {
		this.id = new PersonId(id);

		this.firstName = firstName;
		this.lastName = lastName;
	}

	public Person(PersonId id, String firstName, String lastName) {
		this.id = id;

		this.firstName = firstName;
		this.lastName = lastName;
	}

	public boolean addAddress( String street, String city, String postalCode, String countryCode ) {
		Address newAddress = new Address( street, city, postalCode, countryCode);

		return addresses.add( newAddress);
	}

	public List<AddressDTO> getAddresses() {
		List<AddressDTO> addressesDTO = new ArrayList<AddressDTO>();
		for(Address address : addresses ) {
			AddressDTO addressDTO = new AddressDTO(address.getStreet(), address.getCity(), address.getPostalCode(), address.getCountryCode() );

			addressesDTO.add(addressDTO);
		}
		return addressesDTO;
	}
}
