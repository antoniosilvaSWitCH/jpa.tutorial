package com.example.accessingdatajpa.domain.valueobjects;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class LedgerId {
    @Getter
    private long id;
}