package com.example.accessingdatajpa.services;

import com.example.accessingdatajpa.domain.entities.Country;
import com.example.accessingdatajpa.repositories.CountryRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Propagation;

//import org.springframework.transaction.annotation.Transactional;

@Service
public class CountryService {
    @Autowired
    private CountryRepository countryRepository;

    public CountryService() {
    }

    public Country createAndSaveCountry( String code, String name ) {
        Country country = new Country(code, name);
        return countryRepository.save(country);
    }
/*
    public AddressDTO createAddress(String street, String city, String postalCode, String countryCode) {

        Optional<Country> opCountry = countryRepository.findByCode(countryCode);
        
        if (opCountry.isPresent()) {
            Address newAddress = new Address(street, city, postalCode, opCountry.get());

            Address newAddressSaved = addressRepository.save(newAddress);

            AddressDTO addressDTO = AddressDTOAssembler.createAddressDTO(newAddressSaved.getStreet(),
                                                                        newAddressSaved.getCity(),
                                                                        newAddressSaved.getPostalCode());
            return addressDTO;
        }
        else
            throw new IllegalArgumentException("Country not found!");
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Optional<Country> findByCode(String countryCode) {
        Optional<Country> opCountry = countryRepository.findByCode(countryCode);
        if( opCountry.isPresent()) {
            Country country = opCountry.get();

            List<Address> addrs = country.getAddresses();

            List<Address> newAddrs = new ArrayList<Address>();
            newAddrs.addAll(addrs);
            return opCountry;
        }
        return opCountry;
    }

    @Transactional
    public List<Address> getAddresses(String countryCode)
    {
        Optional<Country> opCountry = countryRepository.findByCode(countryCode);
        if( opCountry.isPresent()) {
            Country country = opCountry.get();

            List<Address> addrs = country.getAddresses();

            List<Address> newAddrs = new ArrayList<Address>();
            newAddrs.addAll(addrs);
            return addrs;
        }
        else
            return null;
    }
    */
}
