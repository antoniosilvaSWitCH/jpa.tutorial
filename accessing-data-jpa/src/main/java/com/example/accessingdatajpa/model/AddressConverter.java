package com.example.accessingdatajpa.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import com.example.accessingdatajpa.model.Address;

@Converter
public class AddressConverter implements AttributeConverter<Address, String> {

    private static final String SEPARATOR = ", ";

    @Override
    public String convertToDatabaseColumn(Address address) {
        if (address == null) {
            return null;
        }

        StringBuilder sb = new StringBuilder();
        if (address.getStreet() != null && !address.getStreet()
                .isEmpty()) {
            sb.append(address.getStreet());
            sb.append(SEPARATOR);
        }

        if (address.getCity() != null && !address.getCity()
                .isEmpty()) {
            sb.append(address.getCity());
            sb.append(SEPARATOR);
        }

        if (address.getPostalCode() != null && !address.getPostalCode()
                .isEmpty()) {
            sb.append(address.getPostalCode());
        }

        return sb.toString();
    }

    @Override
    public Address convertToEntityAttribute(String dbCustomerAddress) {
        if (dbCustomerAddress == null || dbCustomerAddress.isEmpty()) {
            return null;
        }

        String[] pieces = dbCustomerAddress.split(SEPARATOR);

        if (pieces == null || pieces.length == 0) {
            return null;
        }

        Address address = new Address();
        String firstPiece = !pieces[0].isEmpty() ? pieces[0] : null;
        if (dbCustomerAddress.contains(SEPARATOR)) {
            address.setStreet(firstPiece);

            if (pieces.length == 2 && pieces[1] != null && !pieces[1].isEmpty()) {
                address.setCity(pieces[1]);
            }

            if (pieces.length == 3 && pieces[2] != null && !pieces[2].isEmpty()) {
                address.setPostalCode(pieces[2]);
            }

        } else {
            address.setStreet(firstPiece);
        }

        return address;
    }

}