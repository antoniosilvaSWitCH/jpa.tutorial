package com.example.accessingdatajpa.repository;

import com.example.accessingdatajpa.model.Address;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AddressRepository extends CrudRepository<Address, Long> {
    List<Address> findByCity(String city);

    Address findById(long id);
}
