package com.example.accessingdatajpa.controller;

import com.example.accessingdatajpa.dto.AddressDTO;
import com.example.accessingdatajpa.dto.AddressResponseDTO;
import com.example.accessingdatajpa.dto.AddressesResponseDTO;
import com.example.accessingdatajpa.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
public class AddressRESTController {
    @Autowired
    private AddressService service;

    @GetMapping("/address/{addressID}")
    public ResponseEntity<Object> getAddress(@PathVariable String addressID) {
        AddressResponseDTO result = service.findAddress(addressID);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("/addresses")
    public ResponseEntity<Object> getAddresses() {

        AddressesResponseDTO result = service.findAddresses();

        for (String addressID : result.getAddressesIDs()) {
            Link linkToAddress = linkTo(methodOn(AddressRESTController.class).getAddress(addressID)).withRel("addresses");
            result.add(linkToAddress);
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping("/addresses")
    public ResponseEntity<Object> createAddress(@RequestBody AddressDTO info) {
        AddressResponseDTO result = service.createAddress(info.getStreet(), info.getCity(), info.getPostalCode());
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

}
