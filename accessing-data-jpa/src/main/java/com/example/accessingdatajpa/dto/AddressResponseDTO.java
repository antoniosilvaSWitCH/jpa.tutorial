package com.example.accessingdatajpa.dto;

import java.io.Serializable;

public class AddressResponseDTO implements Serializable {

    String id;
    String street;
    String city;
    String postalCode;

    public AddressResponseDTO(Long id, String street, String city, String postalCode) {
        this.id = id.toString();
        this.street = street;
        this.city = city;
        this.postalCode = postalCode;
    }

    public String getId() {
        return id;
    }

    public String getStreet() {
        return street;
    }

    public String getCity() {
        return city;
    }

    public String getPostalCode() {
        return postalCode;
    }

}
