package com.example.accessingdatajpa.dto;

import org.springframework.hateoas.RepresentationModel;

import java.util.Collections;
import java.util.Set;

public class AddressesResponseDTO extends RepresentationModel<AddressesResponseDTO> {

    private Set<String> addressesIDs;

    public AddressesResponseDTO(Set<String> addressesIDs) {
        this.addressesIDs = Collections.unmodifiableSet(addressesIDs);
    }

    public Set<String> getAddressesIDs() {
        return Collections.unmodifiableSet(addressesIDs);
    }
}
