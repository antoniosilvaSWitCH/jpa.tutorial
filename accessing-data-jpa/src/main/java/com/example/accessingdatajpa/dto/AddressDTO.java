package com.example.accessingdatajpa.dto;

import java.io.Serializable;

public class AddressDTO implements Serializable {

    String street;
    String city;
    String postalCode;

    public AddressDTO(String street, String city, String postalCode) {
        this.street = street;
        this.city = city;
        this.postalCode = postalCode;
    }

    public String getStreet() {
        return street;
    }

    public String getCity() {
        return city;
    }

    public String getPostalCode() {
        return postalCode;
    }

}
