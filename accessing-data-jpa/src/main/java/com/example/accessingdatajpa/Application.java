package com.example.accessingdatajpa;

import com.example.accessingdatajpa.model.Address;
import com.example.accessingdatajpa.model.Customer;
import com.example.accessingdatajpa.repository.AddressRepository;
import com.example.accessingdatajpa.repository.CustomerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application implements ApplicationRunner {

	private static final Logger log = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) {
		SpringApplication.run(Application.class);
	}

	@Override
	public void run(ApplicationArguments arg0) throws Exception {
	}

	@Bean
	public CommandLineRunner demo(CustomerRepository customerRepository, AddressRepository addressRepository) {
		return (args) -> {
			// save a few customers
			Address addressA = new Address("Rua das Valongas", "Maia", "4425-401");
			Address addressB = new Address("Rua Dr. Roberto Frias", "Paranhos", "4800-200");
			Address addressC = new Address("Rua das Couves", "Penafiel", "5700-402");

			addressRepository.save(addressA);
			addressRepository.save(addressB);
			addressRepository.save(addressC);

			customerRepository.save(new Customer("Jack", "Bauer", addressA));
			customerRepository.save(new Customer("Chloe", "O'Brian", addressB));
			customerRepository.save(new Customer("Kim", "Bauer", addressA));
			customerRepository.save(new Customer("David", "Palmer", addressC));
			customerRepository.save(new Customer("Michelle", "Dessler", addressB));
			customerRepository.save(new Customer("Rui", "30 caralho", addressC));

			// fetch all customers
			log.info("Customers found with findAll():");
			log.info("-------------------------------");
			for (Customer customer : customerRepository.findAll()) {
				log.info(customer.toString());
			}
			log.info("");

			// fetch an individual customer by ID
//			Customer customer = customerRepository.findById(3L);
//			log.info("Customer found with findById(1L):");
//			log.info("--------------------------------");
//			log.info(customer.toString());
//			log.info("");

			// fetch customers by last name
			log.info("Customer found with findByLastName('Bauer'):");
			log.info("--------------------------------------------");
			customerRepository.findByLastName("Bauer").forEach(bauer -> {
				log.info(bauer.toString());
			});
			log.info("");

			// fetch customers by City
			log.info("Customer found with findByCity('Penafiel'):");
			log.info("--------------------------------------------");
			addressRepository.findByCity("Penafiel").forEach(porto -> {
				log.info(porto.toString());
			});
			log.info("");

		};
	}

}