package com.example.accessingdatajpa.service;

import com.example.accessingdatajpa.model.Address;
import com.example.accessingdatajpa.repository.AddressRepository;
import com.example.accessingdatajpa.dto.AddressResponseDTO;
import com.example.accessingdatajpa.dto.AddressesResponseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class AddressService {

    @Autowired
    private AddressRepository addressRepository;

    public AddressService(AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    public AddressResponseDTO createAddress(String street, String city, String postalCode) {
        Address newAddress = new Address(street, city, postalCode);

        Address newAddressSaved = addressRepository.save(newAddress);

        AddressResponseDTO addressResponseDTO = new AddressResponseDTO(newAddress.getId(), newAddressSaved.getStreet(), newAddressSaved.getCity(), newAddressSaved.getPostalCode());

        return addressResponseDTO;
    }

    public AddressResponseDTO findAddress(String id) {

        Address addressFound = addressRepository.findById(Long.parseLong(id));

        AddressResponseDTO addressResponseDTO = new AddressResponseDTO(addressFound.getId(), addressFound.getStreet(), addressFound.getCity(), addressFound.getPostalCode());

        return addressResponseDTO;
    }

    public AddressesResponseDTO findAddresses() {
        Iterable<Address> addresses = addressRepository.findAll();

        Set<String> adressesIDs = new HashSet<>();

        for (Address address : addresses) {
            adressesIDs.add(address.getId().toString());
        }

        AddressesResponseDTO addressesResponseDTO = new AddressesResponseDTO(adressesIDs);

        return addressesResponseDTO;

    }
}
