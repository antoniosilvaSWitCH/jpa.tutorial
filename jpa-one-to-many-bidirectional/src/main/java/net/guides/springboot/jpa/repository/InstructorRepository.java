package net.guides.springboot.jpa.repository;

import net.guides.springboot.jpa.model.Instructor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface InstructorRepository extends CrudRepository<Instructor, Long> {

}