package net.guides.springboot.jpa;

import net.guides.springboot.jpa.model.Course;
import net.guides.springboot.jpa.model.Instructor;
import net.guides.springboot.jpa.repository.CourseRepository;
import net.guides.springboot.jpa.repository.InstructorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;

@SpringBootApplication
public class Application implements ApplicationRunner {
    @Autowired
    InstructorRepository instructorRepository;
    @Autowired
    CourseRepository courseRepository;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }



    @Override
    public void run(ApplicationArguments args) throws Exception {

        Course c1 = new Course("APROG");
        Course c2 = new Course("PPROG");
        Course c3 = new Course("AMATA");
//=====<NAO PODE GRAVAR Course!!! TEM DE SER O Instructor A GRAVAR
//=====<UM Course SO PODE ESTAR ATRIBUIDO A UM Instructor, SE MUDAR PARA OUTRO Instructor DESAPARECE DO PRIMEIRO

        Instructor instructor1 = new Instructor("Ramesh", "Fadatare", "ramesh@gmail.com");
// c1 e c2 atribuidos a "instructor1"
        instructor1.addCourse(c1);
        instructor1.addCourse(c2);
        instructorRepository.save(instructor1);
// c1 e c2 vao deixar "instructor1" e vao passar para "instructor2"
        Instructor instructor2 = new Instructor("Ramesh2", "Fadatare2", "ramesh2@gmail.com");
        instructorRepository.save(instructor2);
        instructor2.addCourse(c3);
        instructor2.addCourse(c1);
        instructor2.addCourse(c2);
        instructorRepository.save(instructor2);
    }
}
