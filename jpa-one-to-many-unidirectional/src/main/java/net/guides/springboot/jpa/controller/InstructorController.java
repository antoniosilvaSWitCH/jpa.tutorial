package net.guides.springboot.jpa.controller;

import net.guides.springboot.jpa.model.Instructor;
import net.guides.springboot.jpa.repository.InstructorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;


@RestController
public class InstructorController {

    @Autowired
    private InstructorRepository instructorRepository;


    @GetMapping("/instructors")
    public List<Instructor> getInstructors() {
        return (List<Instructor>) instructorRepository.findAll();
    }

    @GetMapping("/instructors/{id}")
    public ResponseEntity<Instructor> getInstructorById(
            @PathVariable(value = "id") Long instructorId) throws ResourceNotFoundException {
        Optional<Instructor> user = instructorRepository.findById(instructorId);
        if (user.isPresent())
            return ResponseEntity.ok().body(user.get());
        else
            throw new ResourceNotFoundException("Instructor not found :: " + instructorId);
    }

    @PostMapping("/instructors")
    public Instructor createUser(@Valid @RequestBody Instructor instructor) {
        return instructorRepository.save(instructor);
    }

    @PutMapping("/instructors/{id}")
    public ResponseEntity<Instructor> updateUser(
            @PathVariable(value = "id") Long instructorId,
            @Valid @RequestBody Instructor userDetails) throws ResourceNotFoundException {
        Optional<Instructor> user = instructorRepository.findById(instructorId);
        if (!user.isPresent())
            throw new ResourceNotFoundException("Instructor not found :: " + instructorId);
        Instructor instructor = user.get();

        instructor.setEmail(userDetails.getEmail());
        final Instructor updatedUser = instructorRepository.save(instructor);
        return ResponseEntity.ok(updatedUser);
    }

    @DeleteMapping("/instructors/{id}")
    public Map<String, Boolean> deleteUser(
            @PathVariable(value = "id") Long instructorId) throws ResourceNotFoundException {
        Optional<Instructor> user = instructorRepository.findById(instructorId);
        if (!user.isPresent())
            throw new ResourceNotFoundException("Instructor not found :: " + instructorId);
        Instructor instructor = user.get();

        instructorRepository.delete(instructor);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
